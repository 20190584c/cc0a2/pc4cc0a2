package pe.uni.v.caycho.jose.pc4cc0a2;

//import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

public class GridAdapter extends BaseAdapter {

    Context context;
    ArrayList<String> arrayTitles;
    ArrayList<Integer> arrayImages;
    ArrayList<String> arrayDescriptions;

    public GridAdapter(Context context, ArrayList<String> arrayTitles, ArrayList<Integer> arrayImages, ArrayList<String> arrayDescriptions) {
        this.context = context;
        this.arrayTitles = arrayTitles;
        this.arrayImages = arrayImages;
        this.arrayDescriptions = arrayDescriptions;
    }

    @Override
    public int getCount() {
        return arrayTitles.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null){
            LayoutInflater layoutInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.item_grid_view, parent, false);
        }
        TextView textViewTitle = convertView.findViewById(R.id.text_view_title);
        ImageView imageView = convertView.findViewById(R.id.image_view);
        TextView textViewDescription = convertView.findViewById(R.id.text_view_description);
        textViewTitle.setText(arrayTitles.get(position));
        imageView.setImageResource(arrayImages.get(position));
        textViewDescription.setText(arrayDescriptions.get(position));
        return convertView;
    }
}
