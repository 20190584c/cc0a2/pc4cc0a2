package pe.uni.v.caycho.jose.pc4cc0a2;

import android.content.Intent;
import android.os.Bundle;
import android.widget.GridView;

import androidx.appcompat.app.AppCompatActivity;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    GridView gridView;

    GridAdapter gridAdapter;
    ArrayList<String> arrayTitles;
    ArrayList<Integer> arrayImages;
    ArrayList<String> arrayDescriptions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        gridView = findViewById(R.id.grid_view);

        arrayTitles = new ArrayList<>();
        arrayImages = new ArrayList<>();
        arrayDescriptions = new ArrayList<>();

        fillArrays();

        gridAdapter = new GridAdapter(MainActivity.this, arrayTitles, arrayImages, arrayDescriptions);
        gridView.setAdapter(gridAdapter);

        gridView.setOnItemClickListener((parent, view, position, id) -> {
            Intent intent = new Intent(MainActivity.this, FormularioActivity.class );
            intent.putExtra(getString(R.string.key_title), arrayTitles.get(position));
            startActivity(intent);
            finish();
        });

    }

    private void fillArrays(){
        String[] at = getResources().getStringArray(R.array.array_titles);
        Integer[] ai = {R.drawable.comida1, R.drawable.comida1, R.drawable.comida1,
                        R.drawable.comida1, R.drawable.comida1, R.drawable.comida1,
                        R.drawable.comida1, R.drawable.comida1};

        String[] ad = getResources().getStringArray(R.array.array_descriptions);

        Collections.addAll(arrayTitles, at);
        Collections.addAll(arrayImages, ai);
        Collections.addAll(arrayDescriptions, ad);

    }
}