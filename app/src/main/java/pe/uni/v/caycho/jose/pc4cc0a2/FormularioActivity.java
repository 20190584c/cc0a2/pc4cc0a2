package pe.uni.v.caycho.jose.pc4cc0a2;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;

public class FormularioActivity extends AppCompatActivity {

    TextView tvTitle;
    EditText etName;
    EditText etDirection;
    Spinner spinner;
    RadioButton rbVisa, rbEfectivo;
    Button btnSend;
    LinearLayout linearLayout;

    ArrayAdapter<CharSequence> adapter;

    String comida;
    String destinatario;
    String direccion;
    String cantidad;
    String pago;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario);

        tvTitle = findViewById(R.id.text_view_title);
        etName = findViewById(R.id.edit_text_name);
        etDirection = findViewById(R.id.edit_text_direction);
        spinner = findViewById(R.id.spinner);
        rbVisa = findViewById(R.id.radio_button_visa);
        rbEfectivo = findViewById(R.id.radio_button_efectivo);
        btnSend = findViewById(R.id.btn_send);
        linearLayout = findViewById(R.id.linear_layout);

        // Title
        Intent intent = getIntent();
        comida = intent.getStringExtra(getString(R.string.key_title));
        tvTitle.setText(comida);

        // spinner
        adapter = ArrayAdapter.createFromResource(FormularioActivity.this, R.array.array_cantidad, android.R.layout.simple_spinner_item);
        spinner.setAdapter(adapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                cantidad = parent.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // Enviar
        btnSend.setOnClickListener(v -> {
            registrarData();
            verificarData();
        });
    }
    private void registrarData(){
        // Destinatario
        destinatario = etName.getText().toString();
        // Dirección
        direccion = etDirection.getText().toString();
        // Opción de pago
        if(rbVisa.isChecked()) pago = getString(R.string.text_rb_visa);
        else if(rbEfectivo.isChecked()) pago = getString(R.string.text_rb_efectivo);
    }

    private void verificarData(){
        if(destinatario == null || direccion == null || cantidad == null || pago == null){
            Snackbar.make(linearLayout, R.string.text_incomplete, Snackbar.LENGTH_LONG)
                    .setTextColor(Color.WHITE)
                    .setBackgroundTint(Color.RED)
                    .show();
            return;
        }

        String message= String.format(getResources().getString(R.string.text_details),
                comida,
                destinatario,
                direccion,
                cantidad,
                pago);

        AlertDialog.Builder builder = new AlertDialog.Builder(FormularioActivity.this);
        builder.setMessage(message)
                .setTitle(R.string.text_alert_dialog_title)
                .setCancelable(false)
                .setPositiveButton(R.string.text_positive, (dialog, which) -> {
                    moveTaskToBack(true);
                    android.os.Process.killProcess(android.os.Process.myPid());
                    System.exit(1);
                })
                .setNegativeButton(R.string.text_negative, (dialog, which) -> {});
        AlertDialog  alert = builder.create();
        alert.show();
    }

}


